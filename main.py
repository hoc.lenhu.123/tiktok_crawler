import requests
import random
from tiktok.config import *
from tiktok.anonymous import TikTokBot
from tiktok.video import Video
from pymongo import MongoClient
import time 
import argparse

# MongoDB
cli = MongoClient(host=MONGO_HOST,
                  port=MONGO_PORT,
                  username=MONGO_USER, password=MONGO_PASSWORD)
col = cli.tiktok.video

# Get keywords by API
def get_keywords():
    url = 'https://sl.sodalab.dev/api/keywords'
    response = requests.get(url)
    return response.json()

def get_video_urls():
    """
    Get video urls by keywords
    Keywords get from api
    """
    bot = TikTokBot(headless=True)
    keywords = get_keywords()
    random.shuffle(keywords)
    for keyword in keywords:
        bot.crawl_video_urls(keyword)
    bot.driver.quit()

def get_video_detail():
    video_bot = Video(proxy=None, headless=True)
    query = {'crawled': None}
    record = col.find_one(query)
    while record:
        start_time = time.time()
        try:
            url = record.get('url')
            item = video_bot.get_info(url)
            item['crawled'] = True
        except Exception as ex:
            item['crawled'] = False
        if not item['num_like']:
            item['crawled'] = False
        total_time = time.time() - start_time
        print(f"{item['crawled']} - {record['video_id']} - {total_time:.3f}s")
        col.update_one({'_id': record['_id']}, {'$set': item})
        record = col.find_one(query)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Crawl tiktok')
    parser.add_argument('--type', type=str, help='url|video', default=None)

    args = parser.parse_args()
    if not args.type:
        get_video_urls()
        get_video_detail()
    elif args.type == 'url':
        get_video_urls()
    else:
        get_video_detail()
