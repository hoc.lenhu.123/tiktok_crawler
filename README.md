# Thu thập dữ liệu TikTok 

* Thu thập dữ liệu video trên tiktok thông qua các từ khóa

* Các từ khóa được lấy từ api:
```
https://sl.sodalab.dev/api/keywords
```

* Quy trình

    * Thu thập video urls theo từ khóa, chạy chương trình:
    ```
    python main.py --type=url
    ```
    
    * Thu thập video chi tiết, chạy chương trình:
    ```
    python main.py --type=video
    ```