import os 
__location__ = os.path.dirname(__file__)
import sys
sys.path.append(__location__)
import undetected_chromedriver.v2 as uc
from selenium.webdriver.common.keys import Keys
import chromedriver_autoinstaller
import time 
import random
from utils.utils import solve_captcha, scroll_down
from utils.bloomfilter import BloomFilter
from config import *
import redis
from pymongo import MongoClient

class TikTokBot:
    def __init__(self, proxy=None, headless=False) -> None:
        chromedriver_autoinstaller.install()
        self.chrome_version = chromedriver_autoinstaller.get_chrome_version().split('.')[0]
        self.driver = self._init_driver(proxy, headless)
        # Config database
        self.db = redis.StrictRedis(
        host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
        self.bloom_filter = BloomFilter(server=self.db, key=VIDEO_BF_KEY)
        cli = MongoClient(host=MONGO_HOST,
                  port=MONGO_PORT,
                  username=MONGO_USER, password=MONGO_PASSWORD)
        self.col = cli.tiktok.video
        # start up
        self.__start_up()

    def _init_driver(self, proxy, headless):
        options = uc.ChromeOptions()
        options.add_argument("--disable-notifications")
        options.add_argument("--disable-popup-blocking")
        options.add_argument("--start-maximized")
        if proxy:
            options.add_argument(f'--proxy-server={proxy}')
        if headless:
            options.add_argument("--headless")
        driver = uc.Chrome(options= options, version_main=self.chrome_version)
        return driver
  
    def __start_up(self):
        print('Starting tiktok . . . . . . . . . . . . . . . . . . . . . . . . . .')
        self.driver.get('https://www.tiktok.com/')
        time.sleep(random.randint(2,3))
        btn_login = self.driver.find_elements('xpath', '//div[@id="login-modal"]//div[@aria-label="Close"]')
        if btn_login:
            btn_login[0].click()
    
    def crawl_video_urls(self, keyword):
        print(f'Searching "{keyword}" . . . . . . . . . . . . .')
        time.sleep(random.randint(3, 5))
        search_input = self.driver.find_element(
            'xpath', '//input[@data-e2e="search-user-input"]')
        reset_search = self.driver.find_elements(
            'xpath', '//div[@data-e2e="reset-search-form"]')
        if reset_search:
            reset_search[0].click()
        time.sleep(random.randint(1, 2))
        search_input.send_keys(keyword)
        search_input.send_keys(Keys.RETURN)
        time.sleep(random.randint(7, 10))
        # CAPTCHA SOLVER
        try:
            solve_captcha(self.driver)
        except Exception as ex:
            print('Load page again!')
            self.driver.get('https://www.tiktok.com/')
            time.sleep(random.randint(2, 3))
            return self.crawl_video_urls(keyword)

        time.sleep(random.randint(3, 5))
        index = 0
        while 1:
            # Get video urls
            a_elments = self.driver.find_elements(
                'xpath', '//a[contains(@href, "/video/")]')
            for a in a_elments[index:]:
                item = {}
                href = a.get_attribute('href')
                if '@' not in href:
                    continue
                item['video_id'] = href.split(
                    '/@')[-1].replace('/video/', '__')
                item['keyword'] = keyword
                item['url'] = href
                if self.bloom_filter.exists(item['video_id']):
                    print(f'EXISTED {href}')
                    continue
                else:
                    self.bloom_filter.insert(item['video_id'])
                    self.col.insert_one(item)
                    print(href)
            if index == len(a_elments):
                break
            else:
                index = len(a_elments)
            # Load more
            btn_load_more = self.driver.find_elements(
                'xpath', '//button[@data-e2e="search-load-more"]')
            scroll_down(self.driver)
            time.sleep(random.randint(2, 3))
            if btn_load_more:
                btn_load_more[0].click()
            else:
                break
            time.sleep(random.randint(2, 3))
            btn_load_more = self.driver.find_elements(
                'xpath', '//button[@data-e2e="search-load-more"]')
