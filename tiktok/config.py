############### Redis Configuration ###############
MONGO_HOST = '103.252.1.144'
MONGO_PORT = 27017
MONGO_USER = 'admin'
MONGO_PASSWORD = 'CIST2o20'

############### Redis Configuration ###############
REDIS_HOST = '103.252.1.150'
REDIS_PORT = 6374
REDIS_DB = 2
VIDEO_QUEUE = 'video_queue'
VIDEO_SEARCH_QUEUE = 'video_search_queue'
USER_QUEUE = 'user_queue'
KEYWORD_QUEUE = 'keyword_queue'

############### Bloom Filter Configuration ###############
BLOOMFILTER_HASH_NUMBER = 6
BLOOMFILTER_BIT = 30
USER_BF_KEY = 'user:bloomfilter'
VIDEO_BF_KEY = 'video:bloomfilter'