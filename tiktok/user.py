import os 
import sys 
__location__ = os.path.dirname(__file__)
sys.path.append(__location__)
from anonymous import TikTokBot
from utils.utils import scroll_down
from utils.bloomfilter import BloomFilter
import time
import random
import redis
from config import *

class User(TikTokBot):
    def __init__(self, proxy=None, headless=False) -> None:
        super().__init__(proxy, headless)
        self.user_id = ''
        self.db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
        self.bloom_filter = BloomFilter(server=self.db, key=USER_BF_KEY)

    def is_user_accessed(self, user_id):
        if user_id != self.user_id:
            url = f'https://www.tiktok.com/@{user_id}'
            self.driver.get(url)
            time.sleep(random.randint(2,3))
            self.user_id = user_id
            return False 
        return True
        
    def get_info(self, user_id):
        print(f'Access new user: {not self.is_user_accessed(user_id)}')
        # Get information
        item = {}
        # User title
        try:
            item['user_title'] = self.driver.find_element('xpath', '//h2[@data-e2e="user-title"]').text
        except Exception as ex:
            item['user_title'] = None
        # User subtitle
        try:
            item['user_subtitle'] = self.driver.find_element('xpath', '//h1[@data-e2e="user-subtitle"]').text
        except Exception as ex:
            item['user_subtitle'] = None 
        # Follow
        try:
            item['following'] = self.driver.find_element('xpath', '//strong[@title="Following"]').text 
        except Exception as ex:
            item['following'] = None 
        # Follower
        try:
            item['followers'] = self.driver.find_element('xpath', '//strong[@title="Followers"]').text
        except Exception as ex:
            item['followers'] = None 
        # Likes
        try:
            item['likes'] = self.driver.find_element('xpath', '//strong[@title="Likes"]').text
        except Exception as ex:
            item['likes'] = None 
        # User bio
        try:
            item['user-bio'] = self.driver.find_element('xpath', '//h2[@data-e2e="user-bio"]').text
        except Exception as ex:
            item['user-bio'] = None 
        return item
    
    def get_suggest_user(self, user_id):
        print(f'Access new user: {not self.is_user_accessed(user_id)}')
        # See all
        btn_see_all = self.driver.find_elements('xpath', '//button[@data-e2e="see-all"]')
        if btn_see_all:
            btn_see_all[0].click()
        time.sleep(2)
        # Crawl suggest user
        a_elements = self.driver.find_elements('xpath', '//a[@data-e2e="suggest-user-avatar"]')
        for a in a_elements:
            href = a.get_attribute('href')
            if '@' not in href:
                continue
            href = href.split('@')[-1]
            # Is duplicated
            if self.bloom_filter.exists(href):
                print(f"EXISTED {href}")
                continue
            else:
                self.bloom_filter.insert(href)
            print(href)
            # Push to queue
            self.db.rpush(USER_QUEUE, href)

    def list_video_urls(self, user_id):
        print(f'Access new user: {not self.is_user_accessed(user_id)}')
        index = 0
        while scroll_down(self.driver):
            time.sleep(random.randint(5,7))
            a_elements = self.driver.find_elements('xpath', "//a[contains(@href, '/video/')]")
            video_urls = [e.get_attribute('href') for e in a_elements[index:]]
            # Push to queue
            for url in video_urls:
                self.db.rpush(VIDEO_QUEUE, url)
                print(url)
            index = len(a_elements) 
            time.sleep(random.randint(2,3))