import os 
__location__ = os.path.dirname(__file__)
import sys 
sys.path.append(__location__)
from utils.utils import scroll_down
from anonymous import TikTokBot
import time
import random
from selenium.webdriver.common.action_chains import ActionChains

class Video(TikTokBot):

    def __init__(self, proxy=None, headless=False) -> None:
        super().__init__(proxy, headless)

    def __get_transcript(self):
        # Click transcript
        btn = self.driver.find_elements('xpath', '//div[contains(@class, "DivIconWrapper")]')
        action = ActionChains(self.driver)
        action.move_to_element(btn[-1]).perform()
        time.sleep(random.randint(1,2))
        btn_transcript = self.driver.find_elements('xpath', '//*[contains(text(), "Transcript")]')
        if btn_transcript:
            action.click(btn_transcript[0]).perform()
        transcript = self.driver.find_elements('xpath', '//ul[contains(@class, "UlTranscriptList")]')
        if transcript:
            return transcript[0].text 
        return None 
    
    def __get_comments(self):
        result = []
        for comment in self.driver.find_elements('xpath', '//div[contains(@class, "DivCommentItemContainer")]'):
            item = {}
            # User
            user = comment.find_elements('xpath', './/a[contains(@class, "StyledUserLinkName")]')
            try:
                item['name'] = user[0].text
            except Exception as ex:
                item['name'] = None 
            try:
                item['user_id'] = user[0].get_attribute('href').split('@')[-1]
            except Exception as ex:
                item['user_id'] = None 
            # Comment
            try:
                item['comment'] = comment.find_element('xpath', './/p[@data-e2e="comment-level-1"]').text
            except Exception as ex:
                item['comment'] = None 
            result.append(item)
        return result

    def get_info(self, video_url):
        self.driver.get(video_url)
        time.sleep(random.randint(2,3))
        item = {}
        # url 
        item['url'] = video_url
        # Description
        try:
            desc = self.driver.find_element('xpath', '//div[@data-e2e="browse-video-desc"]')
            item['desc'] = desc.text
        except Exception as ex:
            item['desc'] = None
        # Hash tag
        try:
            item['hashtag'] = None 
            hashtag = desc.find_elements('xpath', './/a[contains(@href, "/tag/")]')
            if hashtag:
                hashtag = [tag.get_attribute("href").split("/tag/")[-1] for tag in hashtag]
            item['hashtag'] = hashtag
        except Exception as ex:
            item['hashtag'] = None 
        # Like
        try:
            item['num_like'] = self.driver.find_element('xpath', '//strong[contains(@data-e2e,"like-count")]').text 
        except Exception as ex:
            item['num_like'] = None 
        
        # Number of comments
        try:
            item['num_comment'] = self.driver.find_element('xpath', '//strong[contains(@data-e2e,"comment-count")]').text
        except Exception as ex:
            item['num_comment'] = None 

        # Transcript
        try:
            item['transcript'] = self.__get_transcript()
        except Exception as ex:
            item['transcript'] = None

        # Comment
        # scroll_down(self.driver)
        # time.sleep(random.randint(2,3))
        item['comment'] = self.__get_comments()
        return item 
        
        
if __name__ == '__main__':
    video = Video()
    info = video.get_info("https://www.tiktok.com/@duythuongkid/video/7220003220549045510")
    print(info)